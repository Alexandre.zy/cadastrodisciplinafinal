/* A classe ControleAluno cria um controle de instâncias da classe Aluno, 
criando uma lista de alunos e sendo responsável pelo gerenciamento dessa lista */
import java.util.ArrayList;
public class ControleAluno {
	//Atributo
	private ArrayList<Aluno> listaAlunos;
	
	//Construtor
	ControleAluno() {
		listaAlunos = new ArrayList<Aluno>();
	}
	
	//Métodos
	public void adicionaAluno(Aluno aluno) {
		listaAlunos.add(aluno);
		System.out.println("Aluno cadastrado no sistema com sucesso!\n");
	}
	
	public void removeAluno(String nomePesquisado) {
		for(Aluno aluno : listaAlunos) {
			if(aluno.getNomeAluno().equalsIgnoreCase(nomePesquisado))
				listaAlunos.remove(aluno);
		}
	}
	
	public Aluno pesquisaNomeAluno(String nomePesquisado) {
		for(Aluno aluno : listaAlunos) {
			if(aluno.getNomeAluno().equalsIgnoreCase(nomePesquisado)) return aluno;
		}
		return null;
	}
	
	public Aluno pesquisaMatriculaAluno(String matriculaPesquisada) {
		for(Aluno aluno : listaAlunos) {
			if(aluno.getMatriculaAluno().equalsIgnoreCase(matriculaPesquisada)) return aluno;
		}
		return null;
	}
}