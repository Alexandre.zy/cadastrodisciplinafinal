/* Classe responsável por estabelecer uma comunicação entre as classes ControleDisciplina 
e ControleAluno para adicionar disciplinas ao sistema e matricular alunos */
import java.io.*;
public class CadastroAlunosDisciplinas {
	
	public static void main(String[] args) throws IOException {
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;
		char menuOpcao;
		Aluno aluno = new Aluno();
		
		//Responsável por adicionar disciplinas ao sistema e matricular alunos em disciplinas
		ControleDisciplina controleDisciplina = new ControleDisciplina();
		//Responsável por adicionar alunos ao sistema
		ControleAluno controleAluno = new ControleAluno();
		
		System.out.println("---- MATRICULAWEB GENERICO ----\n");
		do {
			System.out.println("*Menu de opcoes do sistema");
			System.out.println("1. Adicionar nova disciplina ao sistema");
			System.out.println("2. Adicionar um novo aluno ao sistema");
			System.out.println("3. Matricular um aluno em uma disciplina");
			//System.out.println("4. Retirar um aluno de uma disciplina");
			//System.out.println("5. Retirar uma disciplina do sistema");
			System.out.println("6. Exibir disciplinas");
			System.out.println("7. Exibir todos os alunos matriculados em uma disciplina");
			System.out.println("8. Sair");
			System.out.println("\nInsira a opcao: ");
			entradaTeclado = leitorEntrada.readLine();
			menuOpcao = entradaTeclado.charAt(0);
			
			switch(menuOpcao) {
				case '1':
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					String umNome = entradaTeclado;
					
					System.out.println("Digite o codigo da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					String umCodigo = entradaTeclado;
					
					Disciplina umaDisciplina = new Disciplina();
					
					umaDisciplina.setNomeDisciplina(umNome);
					umaDisciplina.setCodigoDisciplina(umCodigo);
					
					controleDisciplina.adicionaDisciplina(umaDisciplina);
					break;
				
				case '2':
					System.out.println("Digite o nome do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					String novoNome = entradaTeclado;
					
					System.out.println("Digite a matricula do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					String novaMatricula = entradaTeclado;
					
					Aluno novoAluno = new Aluno(novoNome, novaMatricula);
					controleAluno.adicionaAluno(novoAluno);
					break;
				
				case '3':
					System.out.println("Digite o nome do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					String alunoMatriculado = entradaTeclado;
					
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					String materia = entradaTeclado;
					if(controleAluno.pesquisaNomeAluno(alunoMatriculado) != null){
						aluno = controleAluno.pesquisaNomeAluno(alunoMatriculado);
						controleDisciplina.adicionaAlunoADisciplina(materia, aluno);
					}
					break;
				
				case '6':
					controleDisciplina.exibirDisciplinas();
					break;
				
				case '7':
					System.out.println("Digite o nome da disciplina que quer pesquisar: ");
					entradaTeclado = leitorEntrada.readLine();
					String nomePesquisado = entradaTeclado;
					controleDisciplina.exibirAlunosMatriculados(nomePesquisado);
					break;
				
				default:
					if(menuOpcao != '8')
						System.out.println("Opcao invalida! Digite novamente.");
			}
		} while(menuOpcao != '8');
	}
	
}