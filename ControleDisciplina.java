//Classe responsável por gerenciar uma lista de classes e adicionar alunos às disciplinas da lista
import java.util.ArrayList;
public class ControleDisciplina {
	//Atributo
	private ArrayList<Disciplina> listaDisciplinas;
	
	//Construtor
	ControleDisciplina() {
		listaDisciplinas = new ArrayList<Disciplina>();
	}
	
	//Métodos
	public void adicionaDisciplina(Disciplina disciplina) {
		listaDisciplinas.add(disciplina);
		System.out.println("Disciplina adicionada com sucesso!");
	}
	
	public void adicionaAlunoADisciplina(String disciplinaPesquisada, Aluno alunoPesquisado) {
		for(Disciplina disciplina : listaDisciplinas) {
			if(disciplina.getNomeDisciplina().equalsIgnoreCase(disciplinaPesquisada)){
					disciplina.adicionaAluno(alunoPesquisado);
			}
		}
	}
	
	public void exibirDisciplinas() {
		for(Disciplina disciplina : listaDisciplinas) {
			int i = 1;
			System.out.println(i+". "+disciplina.getCodigoDisciplina()+" - "+disciplina.getNomeDisciplina());
			i++;
		}
	}
	
	public void exibirAlunosMatriculados(String disciplinaPesquisada) {
		for(Disciplina disciplina : listaDisciplinas) {
			if(disciplina.getNomeDisciplina().equals(disciplinaPesquisada)){
				disciplina.exibirAlunosMatriculados();
			}
			break;	
		}
	}
}