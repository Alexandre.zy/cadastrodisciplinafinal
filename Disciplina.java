//Classe disciplina, criada para moldar uma disciplina e atribuir uma lista de alunos matriculados, seu nome e seu código
import java.util.ArrayList;
public class Disciplina {
	//Atributos
	private String nomeDisciplina;
	private String codigoDisciplina;
	private ArrayList<Aluno> matriculadosDisciplina;
	
	//Construtor
	Disciplina() {
		matriculadosDisciplina = new ArrayList<Aluno>();
	}
	
	//Métodos
	public void setNomeDisciplina(String nome) {
		this.nomeDisciplina = nome;
	}
	public String getNomeDisciplina() {
		return this.nomeDisciplina;
	}
	
	public void setCodigoDisciplina(String codigo) {
		this.codigoDisciplina = codigo;
	}
	public String getCodigoDisciplina() {
		return this.codigoDisciplina;
	}
	
	public void adicionaAluno(Aluno aluno) {
		matriculadosDisciplina.add(aluno);
		System.out.println("Aluno matriculado com sucesso!\n");
	}
	public void removeAluno(String nome) {
		for(Aluno aluno : matriculadosDisciplina) {
			if(aluno.getNomeAluno().equalsIgnoreCase(nome))
				matriculadosDisciplina.remove(aluno);
		}
	}
	
	public void exibirAlunosMatriculados() {
		for(Aluno aluno : matriculadosDisciplina) {
			System.out.println(aluno.getNomeAluno()+" - "+aluno.getMatriculaAluno());
		}
	}
}