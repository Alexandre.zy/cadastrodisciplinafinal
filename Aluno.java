/* A classe aluna procura moldar um aluno, atribuindo um nome e uma matrícula às suas instâncias, 
	além dos métodos de acesso à classe (interface da classe) */

public class Aluno {
	//Atributos
	private String nomeAluno;
	private String matriculaAluno;
	
	//Construtores
	Aluno() {
	}
	Aluno(String nome, String matricula) {
		this.nomeAluno = nome;
		this.matriculaAluno = matricula;
	}
	
	//Métodos
	public void setNomeAluno(String nome) {
		this.nomeAluno = nome;
	}
	public String getNomeAluno() {
		return this.nomeAluno;
	}
	
	public void setMatriculaAluno(String matricula) {
		this.matriculaAluno = matricula;
	}
	public String getMatriculaAluno() {
		return this.matriculaAluno;
	}
}